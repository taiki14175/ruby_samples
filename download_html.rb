require 'open-uri'
require 'open_uri_redirections'

now = Time.now.strftime("%Y%m%d%H%M")
url = 'http://www.kddi.com/corporate/ir/'
open(url, allow_redirections: :safe) do |uri|
  File.open(now, 'w') do |html|
    html.puts(uri.read)
  end
end
