require 'open-uri'
require 'open_uri_redirections'

# url = 'http://www.kddi.com/corporate/ir/'
url = 'http://www.kddi.co'
# puts open(url, allow_redirections: :safe).read
begin
  puts open(url, allow_redirections: :safe)
rescue SocketError => ex
  puts 'SocketError'
  puts '指定されたURLは存在しません。'
end
