require 'diffy'

# diffing files does not work
# puts Diffy::Diff.new("#{dir}/index2.html", "#{dir}/index2.html", sources: :files)

dir = 'diffy_sample'
# html_old = File.open("#{dir}/same1").read
# html_new = File.open("#{dir}/same2").read

html_old = File.open("#{dir}/kddi1").read
html_new = File.open("#{dir}/kddi2").read

puts Diffy::Diff.new(html_old, html_new)
